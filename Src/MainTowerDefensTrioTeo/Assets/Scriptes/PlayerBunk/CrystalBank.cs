using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BankFromCrystal", menuName = "BoxFromCrystal")]
public class CrystalBank: ScriptableObject
{
    [Range(0,10000)]
    public int Resources;
}
