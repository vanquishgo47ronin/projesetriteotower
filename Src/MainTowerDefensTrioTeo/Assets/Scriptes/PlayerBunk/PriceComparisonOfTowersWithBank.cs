using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class PriceComparisonOfTowersWithBank
{
    private CrystalBank _crystalBank;

    private Button _machineTower;
    private Button _antiTankTower;
    private Button _plazmaganTower;
    private Button _antiFlyTower;

    //private TextMeshProUGUI _textBnkCrystal;
    private TextMeshProUGUI _texstMachine;
    private TextMeshProUGUI _texstAntiTank;
    private TextMeshProUGUI _texstPlazmagan;
    private TextMeshProUGUI _texstAntiFly;

    private ButtonFromBilldingTower _buttonFromBilldingTower;

    public PriceComparisonOfTowersWithBank(CrystalBank crystalBank, /*TextMeshProUGUI textBnkCrystal,*/ TextMeshProUGUI texstMachine, TextMeshProUGUI texstAntiTank, TextMeshProUGUI texstPlazmagan, TextMeshProUGUI texstAntiFly, ButtonFromBilldingTower buttonFromBilldingTower,
        Button machineTower, Button antiTankTower, Button plazmaganTower, Button antiFlyTower)
    {
        //_textBnkCrystal = textBnkCrystal;
        _texstMachine = texstMachine;
        _texstAntiTank = texstAntiTank;
        _texstPlazmagan = texstPlazmagan;
        _texstAntiFly = texstAntiFly;
        _crystalBank = crystalBank;
        _buttonFromBilldingTower = buttonFromBilldingTower;

        _machineTower = machineTower;
        _antiTankTower = antiTankTower;
        _plazmaganTower = plazmaganTower;
        _antiFlyTower = antiFlyTower;
    }

    public PriceComparisonOfTowersWithBank()
    {

    }

    public void ComparePricesAtMachine()
    {
        if (/*(int)_priceMachine*/ _crystalBank.Resources >= 100)
        {
            _texstMachine.color = new Color32 (255,255,255,255);
            _machineTower.interactable = true;
        }
        else if (_crystalBank.Resources < 100)
        {
            _texstMachine.color = new Color32 (255, 32, 0, 255);
            _buttonFromBilldingTower.DisableButtonsMachineTower();
        }
    }

    public void ComparePricesAtAntiTank()
    {
        if (_crystalBank.Resources >= 210)
        {
            _texstAntiTank.color = new Color32(255, 255, 255, 255);
            _antiTankTower.interactable = true;
        }
        else if (_crystalBank.Resources < 210)
        {
            _texstAntiTank.color = new Color32(255, 32, 0, 255);
            _buttonFromBilldingTower.DisableButtonsAntiTank();
        }
    }

    public void ComparePricesAtPlazmagan()
    {
        if (_crystalBank.Resources >= 350)
        {
            _texstPlazmagan.color = new Color32(255, 255, 255, 255);
            _plazmaganTower.interactable = true;
        }
        else if (_crystalBank.Resources < 350)
        {
            _texstPlazmagan.color = new Color32(255, 32, 0, 255);
            _buttonFromBilldingTower.DisableButtonsPlazmaganTower();
        }
    }

    public void ComparePricesAtAntiFly()
    {
        if (_crystalBank.Resources >= 150)
        {
            _texstAntiFly.color = new Color32(255, 255, 255, 255);
            _antiFlyTower.interactable = true;
        }
        else if (_crystalBank.Resources < 150)
        {
            _texstAntiFly.color = new Color32(255, 32, 0, 255);
            _buttonFromBilldingTower.DisableButtonsAntiFlyTower();
        }
    }

    // ���������� ��������� � ���� ����� ������ ������.
    public void AddCristall()
    {
        /*if ()
        {
            _crystalBank.Resources += 5;
        }
        else if ()
        {
            _crystalBank.Resources += 30;
        }
        else if ()
        {
            _crystalBank.Resources += 15;
        }*/
    }
}
