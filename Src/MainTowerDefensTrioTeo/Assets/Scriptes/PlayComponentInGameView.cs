using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class PlayComponentInGameView : MonoBehaviour
{
    [SerializeField] private CrystalBank _crystalBank;
    [SerializeField] private TextMeshProUGUI _textLife;
    [SerializeField] private TextMeshProUGUI _textBnkCrystal;
    [SerializeField] private Button _machineTower;
    [SerializeField] private Button _antiTankTower;
    [SerializeField] private Button _plazmaganTower;
    [SerializeField] private Button _antiFlyTower;
    [SerializeField] private Image _Ray;
    [SerializeField] private Image _gunTrio;
    [SerializeField] private TextMeshProUGUI _priceMachine;
    [SerializeField] private TextMeshProUGUI _priceAntiTank;
    [SerializeField] private TextMeshProUGUI _pricePlazmagan;
    [SerializeField] private TextMeshProUGUI _priceAntiFly;

    [SerializeField] private ButtonFromBilldingTower _buttonFromBilldingTower;

    [SerializeField] private PlatformForTower _platformForTower;

    private PriceComparisonOfTowersWithBank _priceComparisonOfTowersWithBank;
    private BuildingTower _buildingTower;

    private void Awake()
    {
        //_platformForTower.AssignValue(_machineTower, _antiTankTower, _plazmaganTower, _antiFlyTower);

        _buildingTower = new BuildingTower(_platformForTower);

        _priceComparisonOfTowersWithBank = new PriceComparisonOfTowersWithBank(_crystalBank, _priceMachine, _priceAntiTank, _pricePlazmagan, _priceAntiFly, _buttonFromBilldingTower, 
            _machineTower, _antiTankTower, _plazmaganTower, _antiFlyTower);
    }

    // Update is called once per frame
    public void UpdateColorTextPriceMachine()
    {
        //_priceMachine.vert;
    }

    private void Update()
    {
        _priceComparisonOfTowersWithBank.ComparePricesAtMachine();
        _priceComparisonOfTowersWithBank.ComparePricesAtAntiTank();
        _priceComparisonOfTowersWithBank.ComparePricesAtPlazmagan();
        _priceComparisonOfTowersWithBank.ComparePricesAtAntiFly();
        _textBnkCrystal.text = _crystalBank.Resources.ToString();
    }
}
