using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using NamTowers;

public class ButtonFromBilldingTower : MonoBehaviour
{
    /*[SerializeField] private DataMachineTower _towerMachineLevelOne;
    [SerializeField] private DataAntiTank _towerAntiTankLevelOne;
    [SerializeField] private DataPlazmaganTower _towerPlazmaganLevelOne;
    [SerializeField] private DataAntiTank _towerAntiFlyLevelOne;*/
    //[SerializeField] private DataTransfer _dataTransfer;
    //private BuildingTower _buildingTower;
    //_buildingTower = new BuildingTower(_platformForTower);
    //private TowerSelection _towerSelection;
    //[SerializeField] private Button _buttonFromBilld;
    [SerializeField] private DataMachineTower _dataMachineTower;
    [SerializeField] private DataAntiTank _dataAntiTank;
    [SerializeField] private DataPlazmaganTower _dataPlazmaganTower;
    [SerializeField] private DataAntiFlyTower _dataAntiAirStrikeFlyTower;

    [SerializeField] private CrystalBank _crystalBank;

    [SerializeField] private Button _buttonMachine;
    [SerializeField] private Button _buttonAntiTank;
    [SerializeField] private Button _buttonPlazmagan;
    [SerializeField] private Button _buttonAntiFly;

    private BuildingTower _buildingTower;

    private PriceComparisonOfTowersWithBank _priceComparisonOfTowersWithBank;

    TowerSelection _towerSelection;
    [SerializeField] private Button _button;
    [SerializeField] private NameTowers _name;

    [SerializeField] private PlatformForTower _platformForTower;

    private void Awake()
    {
        _priceComparisonOfTowersWithBank = new PriceComparisonOfTowersWithBank();
        _buildingTower = new BuildingTower(_dataMachineTower, _dataAntiTank, _dataPlazmaganTower, _dataAntiAirStrikeFlyTower/*, _towerMachineLevelOne, _towerAntiTankLevelOne, _towerPlazmaganLevelOne, _towerAntiFlyLevelOne*/);
        //_dataTransfer = new DataTransfer(_buildingTower);
        _towerSelection = new TowerSelection();
    }

    public void SendLinkFromBuildTower()
    {
        if (/*actualPosition == true && */_crystalBank.Resources >= 100/*_textMachine.color == Color.white*/)
        {
            _buttonMachine.interactable = true;
            for (int i = 0; i < _dataMachineTower.ModelTowersMachine.Count;)
            {
                if (i == 0)
                {
                    GameObject _firstMachineTower = _dataMachineTower.ModelTowersMachine[0];
                    _platformForTower.StartBuilding(_firstMachineTower);

                }
                break;
            }
            _towerSelection.ChooseTower(_name, _buildingTower);
        }
        else if (/*actualPosition == false &&*/ _crystalBank.Resources < 100/*_textMachine.color == Color.red*/)
        {
            _buttonMachine.interactable = false;
        }

        if (/*actualPosition == true &&*/ _crystalBank.Resources >= 210/*_textAntiTank.color == Color.white*/)
        {
            _buttonAntiTank.interactable = true;
            _towerSelection.ChooseTower(_name, _buildingTower);
        }
        else if (/*actualPosition == false &&*/ _crystalBank.Resources < 210/*_textAntiTank.color == Color.red*/)
        {
            _buttonAntiTank.interactable = false;
        }

        if (/*actualPosition == true &&*/ _crystalBank.Resources >= 350/*_textPlazmagan.color == Color.white*/)
        {
            _buttonPlazmagan.interactable = true;
            _towerSelection.ChooseTower(_name, _buildingTower);
        }
        else if (/*actualPosition == false &&*/ _crystalBank.Resources < 350/*_textPlazmagan.color == Color.red*/)
        {
            _buttonPlazmagan.interactable = false;
        }

        if (/*actualPosition == true &&*/ _crystalBank.Resources >= 150/*_textAntiTank.color == Color.white*/)
        {
            _buttonAntiFly.interactable = true;
            _towerSelection.ChooseTower(_name, _buildingTower);
        }
        else if (/*actualPosition == false &&*/ _crystalBank.Resources < 150/*_textAntiTank.color == Color.red*/)
        {
            _buttonAntiFly.interactable = false;
        }
        
    }

    public void DisableButtonsMachineTower()
    {
        _buttonMachine.interactable = false;
    }

    public void DisableButtonsAntiTank()
    {
        _buttonAntiTank.interactable = false;
    }

    public void DisableButtonsPlazmaganTower()
    {
        _buttonPlazmagan.interactable = false;
    }

    public void DisableButtonsAntiFlyTower()
    {
        _buttonAntiFly.interactable = false;
    }

    /*private void ChooseTower(NameTowers namTow)
    {
        if (namTow == NameTowers.Machine_Tower)
        {
            Debug.Log("� ������!");
        }
        else if (namTow == NameTowers.AntiTank)
        {
            Debug.Log("� ��������������� ������!");
        }
        else if (namTow == NameTowers.Plazmagan)
        {
            Debug.Log("� ���������!");
        }
        else if (namTow == NameTowers.AntiFly)
        {
            Debug.Log("� ���!");
        }
    }*/
}
