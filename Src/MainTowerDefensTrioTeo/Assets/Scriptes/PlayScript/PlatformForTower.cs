using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;
using NamTowers;
using TMPro;

public class PlatformForTower : MonoBehaviour //, IPointerClickHandler
{
    //[SerializeField] private Transform _areaFromTower;
    

    private PlatformForTower _platformForTower;
    /*[SerializeField] private TextMeshProUGUI _textMachine;
    [SerializeField] private TextMeshProUGUI _textAntiTank;
    [SerializeField] private TextMeshProUGUI _textPlazmagan;
    [SerializeField] private TextMeshProUGUI _textAntiFly;*/

    [SerializeField] private Transform _platform;
    
    private bool _actualPosition = false;

    private bool _readyForConstruction;

    /*public void AssignValue(Button buttonFromBildingMachine, Button buttonFromBildingAntiTank, Button buttonFromBildingPlazmagan, Button buttonFromBildingAntiFly)
    {
        //Debug.Log(buttonFromBilding);
        _buttonMachine = buttonFromBildingMachine;
        _buttonAntiTank = buttonFromBildingAntiTank;
        _buttonPlazmagan = buttonFromBildingPlazmagan;
        _buttonAntiFly = buttonFromBildingAntiFly;
    }*/

     private void OnMouseDown()
     {
            //_actualPosition = true;
            _readyForConstruction = true;
            //CompareConditions(_actualPosition /*, _buttonMachine, _buttonAntiTank, _buttonPlazmagan, _buttonAntiFly*/ //);
     }

    /*public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.pointerId == -1)
        {
            GetComponent<Renderer>().material.color = Color.white;
            _readyForConstruction = true;
            _actualPosition = true;
            //CompareConditions(_actualPosition , _buttonMachine, _buttonAntiTank, _buttonPlazmagan, _buttonAntiFly);
            _actualPosition = false;
        }
    }*/

    //private void CompareConditions(bool actualPosition ,Button buttonFromBildMachine, Button buttonFromBildAntiTank, Button buttonFromBildPlazmagan, Button buttonFromBildAntiFly)
    //{
        //   Debug.Log("CompareConditions");
        // Debug.Log(buttonFromBild);

        //if (/*actualPosition == true && */_crystalBank.Resources >= 100/*_textMachine.color == Color.white*/)
       // {
           // _buttonMachine.interactable = true;
        //}
        //else if (/*actualPosition == false &&*/ _crystalBank.Resources < 100/*_textMachine.color == Color.red*/)
        //{
        //    _buttonMachine.interactable = false;
        //}

        //if (/*actualPosition == true &&*/ _crystalBank.Resources >= 210/*_textAntiTank.color == Color.white*/)
        //{
           // _buttonAntiTank.interactable = true;
        //}
        //else if (/*actualPosition == false &&*/ _crystalBank.Resources < 210/*_textAntiTank.color == Color.red*/)
        //{
            //_buttonAntiTank.interactable = false;
        //}

        //if (/*actualPosition == true &&*/ _crystalBank.Resources >= 350/*_textPlazmagan.color == Color.white*/)
        //{
            //_buttonPlazmagan.interactable = true;
        //}
        //else if (/*actualPosition == false &&*/ _crystalBank.Resources < 350/*_textPlazmagan.color == Color.red*/)
        //{
           // _buttonPlazmagan.interactable = false;
        //}

        ///if (/*actualPosition == true &&*/ _crystalBank.Resources >= 150/*_textAntiTank.color == Color.white*/)
        //{
            //_buttonAntiFly.interactable = true;
        //}
        //else if (/*actualPosition == false &&*/ _crystalBank.Resources < 150/*_textAntiTank.color == Color.red*/)
        //{
            //_buttonAntiFly.interactable = false;
        //}
    //}

    

    public void StartBuilding(GameObject obj)
    {
        if (_readyForConstruction)
        {
            Instantiate(obj, _platform.transform.position, _platform.transform.rotation);
            //_platform.gameObject.SetActive(false);
            _readyForConstruction = false;
        }
    }
}
