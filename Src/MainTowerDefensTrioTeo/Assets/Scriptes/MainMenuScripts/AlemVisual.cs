using System.Collections;
using System.Collections.Generic;
using System.Windows.Input;
using UnityEngine.UI;
using UnityEngine;

public class AlemVisual : MonoBehaviour
{
    // ���� (������ �� ������)
    ActiveButton _activeButton;
    //ButtonTransititionNumberOne _buttonTransititionNumberOne;

    // Canvas
    [SerializeField] private Canvas _mainMenu;
    [SerializeField] private Canvas _mainImproveman;
    [SerializeField] private Canvas _mainLevels;

    // Buttons
    [SerializeField] private Button _buttonPlay;
    [SerializeField] private Button _buttonBackInMainMenu;
    [SerializeField] private Button _buttonBackInMenuLevel;
    [SerializeField] private Button _buttonExit;

    // Levels
    [SerializeField] private Image _itemLevelOne;
    [SerializeField] private Image _itemLevelTwo;
    [SerializeField] private Image _itemLevelThree;
    [SerializeField] private Image _itemLevelFore;
    [SerializeField] private Image _itemLevelFive;

    // Improvement (���������)
    /*
    [SerializeField] private Image _itemImprovementRayLevelTwo;
    [SerializeField] private Image _itemImprovementRayLevelThree;
    [SerializeField] private Image _itemImprovementRayLevelFore;
    [SerializeField] private Image _itemImprovementRayLevelFive;
    [SerializeField] private Image _itemImprovementGunTrioLevelTwo;
    [SerializeField] private Image _itemImprovementGunTrioLevelThree;
    [SerializeField] private Image _itemImprovementGunTrioLevelFore;
    [SerializeField] private Image _itemImprovementGunTrioLevelFive;
    */

    public System.Action action; // �������

    private void Awake()
    {
        _activeButton = new ActiveButton(_mainMenu, _mainImproveman, _mainLevels, _buttonPlay, _buttonBackInMainMenu, _buttonExit);
        /*
        _buttonPlay.onClick.RemoveAllListeners();
        //_buttonPlay.onClick.AddListener(delegate { On});
        _buttonPlay.onClick.AddListener(_activeButton.OnClikePlay);

        _buttonBack.onClick.RemoveAllListeners();
        //_buttonBack.onClick.AddListener(_activeButton);
        _buttonBack.onClick.AddListener(_activeButton.OnClikeBack);
        */
    }

    /*private void Start()
    {
        _buttonPlay.onClick.AddListener(_activeButton.OnClikePlay);
        _buttonBackInMainMenu.onClick.AddListener(_activeButton.OnClikeBackInMainMenu);
        _buttonBackInMenuLevel.onClick.AddListener(_activeButton.OnClikeBackInMenuLevel);
    }*/

    public void OnCklikeButtonPlay()
    {
        _activeButton.OnClikePlay();

    }

    public void OnCklikeButtonBackInMenuLevel()
    {
        _activeButton.OnClikeBackInMenuLevel();
    }

    public void OnCklikeButtonBackInMainMenu()
    {
        _activeButton.OnClikeBackInMainMenu();
    }

    public void OnCklickeButtonImproveman()
    {
        _activeButton.OnCklickeImproveman();
    }
}
