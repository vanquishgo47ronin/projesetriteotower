using System.Collections.Generic;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;
using SceneList;
using System;


public class ButtonTransitition : MonoBehaviour, IPointerClickHandler, IPointerExitHandler
{
    [SerializeField] private Image _image;
    [SerializeField] private Scenes _scene;

    public void OnPointerClick(PointerEventData eventData)
    {
        _image.transform.localScale = new Vector2(1.1f, 1.1f);
        ChacksForAmatch(_scene);
    }

    private void ChacksForAmatch(Scenes sce)
    {
        if (sce == Scenes.LEVEL1)
        {
            //_sceneManage.LetIsMoveOnFifthLevels();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else if (sce == Scenes.LEVEL2)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
        }
        else if (sce == Scenes.LEVEL3)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 3);
        }
        else if (sce == Scenes.LEVEL4)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 4);
        }
        else if (sce == Scenes.LEVEL5)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 5);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _image.transform.localScale = new Vector2(1f, 1f);
    }
}

