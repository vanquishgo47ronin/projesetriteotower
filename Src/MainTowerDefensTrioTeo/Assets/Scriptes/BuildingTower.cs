using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using NamTowers;
using System;

public class BuildingTower
{
    //private NameTowers _nameTower;

    //private Building _building;

    private GameObject _firstMachineTower;

    private PlatformForTower _platformForTower;

    /*private DataMachineTower _towerMachineLevelOne; 
    private DataAntiTank _towerAntiTankLevelOne; 
    private DataPlazmaganTower _towerPlazmaganLevelOne; 
    private DataAntiTank _towerAntiFlyLevelOne;*/

    private DataMachineTower _dataMachineTower;
    private DataAntiTank _dataAntiTank;
    private DataPlazmaganTower _dataPlazmaganTower;
    private DataAntiFlyTower _dataAntiFlyTower;

    /*private List<GameObject> _listMachineTower;
    private List<GameObject> _listAntiTank;
    private List<GameObject> _listPlazmaganTower;
    private List<GameObject> _listAntiFlyTower;*/

    public BuildingTower(DataMachineTower dataMachineTower, DataAntiTank dataAntiTank, DataPlazmaganTower dataPlazmaganTower, DataAntiFlyTower dataAntiFlyTower/*, PlatformForTower platformForTower*/)
        /*,GameObject towerMachineLevelOne, GameObject towerAntiTankLevelOne, GameObject towerPlazmaganLevelOne, GameObject towerAntiFlyLevelOne*/ 
        /*,DataMachineTower towerMachineLevelOne, DataAntiTank towerAntiTankLevelOne, DataPlazmaganTower towerPlazmaganLevelOne, DataAntiFlyTower towerAntiFlyLevelOne*/
    {
        _dataMachineTower = dataMachineTower;
        _dataAntiTank = dataAntiTank;
        _dataPlazmaganTower = dataPlazmaganTower;
        _dataAntiFlyTower = dataAntiFlyTower;

        //_platformForTower = platformForTower;

        /*Debug.Log("PlatformForTower");
        Debug.Log(_platformForTower);*/

        //_building = building;
        /*_towerAntiTankLevelOne = towerAntiTankLevelOne;
        _towerMachineLevelOne = towerMachineLevelOne;
        _towerPlazmaganLevelOne = towerPlazmaganLevelOne;
        _towerAntiFlyLevelOne = towerAntiFlyLevelOne;*/

        /*_dataMachineTower = towerMachineLevelOne;
        _dataAntiTank = towerAntiTankLevelOne;
        _dataPlazmaganTower = towerPlazmaganLevelOne;
        _dataAntiFlyTower = towerAntiFlyLevelOne;*/
    }

    /*public BuildingTower()//Building building)
    {
        //this._building = building;
        
        
    }*/

    /*public BuildingTower()//Building building)
    {
        //this._building = building;
        
        
    }*/

    public BuildingTower(PlatformForTower platformForTower )//Building building)
    {
        //this._building = building;
        
        _platformForTower = platformForTower;
        Debug.Log(_platformForTower);
    }

    public void ChooseAtTower(NameTowers nameTower)
    {
        if (nameTower == NameTowers.MachineTower)
        {
            BuildMachineTower();
        }
        else if (nameTower == NameTowers.AntiTank)
        {
            BuildAntiTankTower();
        }
        else if (nameTower == NameTowers.Plazmagan)
        {
            BuildPlazmaganTower();
        }
        else if (nameTower == NameTowers.AntiFly)
        {
            BuildAntiFlyeTower();
        }
    }

    private void BuildMachineTower()
    {
        //foreach (DataTowerMachine dataTower in _dataMachineTower.TowerMachine)
        //{
        for (int i = 0; i < _dataMachineTower.ModelTowersMachine.Count;)
        {
            if (i == 0)
            {
                GameObject _firstMachineTower = _dataMachineTower.ModelTowersMachine[0];
                //Debug.Log(_platformForTower);
                _platformForTower.StartBuilding(_firstMachineTower);

            }
            break;
        }
        //}
    }

    /*private void BuildUpMachineTower()
    {
        int indexMachine;
    }*/

    private void BuildAntiTankTower()
    {
        for (int i = 0; i < _dataAntiTank.ModelTowersAntiTank.Count;)
        {
            if (i == 0)
            {
                GameObject _firstAntiTankTower = _dataAntiTank.ModelTowersAntiTank[0];
                _platformForTower.StartBuilding(_firstAntiTankTower);
            }
            break;
        }
        /*for (int i = 0; i < _listAntiTank.Count; i++)
        {
            if (i == 0)
            {
                break;
            }
        }*/
    }

    /*private void BuildUpTankTower()
    {

    }*/

    private void BuildPlazmaganTower()
    {
        for (int i = 0; i < _dataPlazmaganTower.ModelTowersPlazmagan.Count;)
        {
            if (i == 0)
            {
                GameObject _firstPlazmaganTower = _dataPlazmaganTower.ModelTowersPlazmagan[0];
                _platformForTower.StartBuilding(_firstPlazmaganTower);
            }
            break;
        }
    }

    /*private void BuildUpPlazmaganTower()
    {

    }*/

    private void BuildAntiFlyeTower()
    {
        for (int i = 0; i < _dataAntiFlyTower.ModelTowersAntiFly.Count;)
        {
            if (i == 0)
            {
                GameObject _firstTowerAntiFlyeTower = _dataAntiFlyTower.ModelTowersAntiFly[0];
                _platformForTower.StartBuilding(_firstTowerAntiFlyeTower);
            }
            break;
        }
    }

    /*private void BuildUpAntiFlyeTower()
    {

    }*/

    
}
